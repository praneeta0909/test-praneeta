package com.scholastic.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * API Endpoint for Heartbeat.
 *
 * @author Ketan Maydeo
 */
@RestController
public class PingController {

    @GetMapping(path = "/ping")
    public String ping() {
        return "pong";
    }
}
