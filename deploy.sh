#!/usr/bin/env bash
#docker-machine create -d virtualbox mymachine
#docker-machine start mymachine
eval $(docker-machine env mymachine)
docker rmi -f $(docker images -q -f dangling=true) | xargs --no-run-if-empty
docker rmi -f $(docker images | grep "^is/spring-boot-demo" | awk '{print $3}')
is/spring-boot-demo
./mvnw install -DskipTests
#docker run -p 8080:8080 -t is/spring-boot-demo